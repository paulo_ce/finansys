import { InMemoryDbService } from "angular-in-memory-web-api";

export class InMemoryDatabase implements InMemoryDbService {
    createDb(){
        const categories = [
            { id: 1, name: 'Moradia', description: 'Pagamentos de conta da casa'},
            { id: 2, name: 'Saúde', description: 'Pagamentos de Saúde e Remédios'},
            { id: 1, name: 'Lazer', description: 'Cinema, parques, praia, etc...'},
            { id: 1, name: 'Salário', description: 'Recebimento de Salário'},
            { id: 1, name: 'Freelas', description: 'Trabalhos como freelancer'}
        ];

        return {categories}
    }
}